
CONTENTS OF THIS FILE
=====================
- WHAT ARE PODGROUPS
- USING PODGROUPS
- TROUBLESHOOTING
- CREDITS


WHAT ARE PODGROUPS
==================
Podgroups are a CCK group type that all content creators to define nested tree
data structures comprised of instances of the defined podgroups.  Depending on
the configuration options selected, content creators can place these nested
structures in regions defined in the theme.


USING PODGROUPS
===============
To use Podgroups, create a CCK group of type 'Podgroup' and add one or more
fields to it.  Due to logic in CCK and Podgroups, please ensure that each
podgroup contains at least one required field... this can be accomplished via
a standard required field, a required hidden field with a default value, or a
validation rule that verifies at least one field is filled in per podgroup.
When it comes to theming, copy the content_podgroup.tpl.php to your theme, you
can also add your own template suggestions via the preprocess function.

IMPORTANT NOTE: Currently Javascript must be enabled for editing to function...
adding support for Javascript disabled browsers is on the to do list.


TROUBLESHOOTING
===============

Most issues reported during developement were related to Podgroups that did not
have at least one required field (Please see the note above in the 'Using 
Podgroups' section).  The other issue that arose during development was that
some Javascript enabled form widgets experienced issues due to race conditions
in the Javascript functions that occur after the ahah calls.  If you experience
any issues please submit an issue ticket and include detailed steps to reproduce
your issue.


CREDITS
=======
Mad props go to the CCK module creators/maintainers and all those that have
contributed to the CCK Multigroup module (CCK 3.x).  
